#define _DEFAULT_SOURCE
#include "test.h"
#include "mem.h"

#include <unistd.h>
#include <stdbool.h>

#define BLOCK_MIN_CAPACITY 24
#define QUERY0 16
#define QUERY1 128
#define QUERY2 512

 bool test1(void* heap) {
    /**
     * In this test, we will test our own malloc function by request 2 chunk of memory.
     * - First chunk: 16 bytes, which is smaller than BLOCK_MIN_CAPACITY. The allocator should allocate BLOCK_MIN_CAPACITY bytes for this one.
     * - Second request: 128 bytes, which is bigger than BLOCK_MIN_CAPACITY. Tha allocator should allocate 128 bytes for this one.
     * After checking the size of our pointer, we must free the memory to other test can reuse the heap.
     */
    printf("Test 1: Simple allocation\n");

    // Let's allocate some memory
    void* ptr0 = _malloc(QUERY0);
    void* ptr1 = _malloc(QUERY1);

    if (ptr0 == NULL || ptr1 == NULL) err("Allocation failed. Received NULL pointer .\n");

    // Get address of header from address of contents
    struct block_header* header0 = block_get_header(ptr0);
    struct block_header* header1 = block_get_header(ptr1);

    // Check if the capacity is expected value
    if (header0->is_free || header0->capacity.bytes != BLOCK_MIN_CAPACITY) {
        printf("Failed! Allocator allocated memory smaller than BLOCK_MIN_CAPACITY.\n");
        return false;
    }

    if (header1->is_free || header1->capacity.bytes != QUERY1) {
        printf("Failed! Allocator allocated memory not equal to requested size.\n");
        return false;
    }

    // Print the map of the heap
    debug_heap(stdout, heap);

    // Free the allocated memory
    _free(ptr0);
    _free(ptr1);

    printf("==> Test 1 passed.\n\n");
    return true;
}

 bool test2(void* heap) {
    /**
     * In this test, we will test our own malloc by allocating 3 chunk of memory and then free the third one.
     * After freeing the 3rd block, the allocator should merge the 3rd block to the free block next to it.
     * If we don't see the merge, then the allocator failed this test.
     */
    printf("Test 2: Free 1 block\n");

    void* ptr0 = _malloc(QUERY0);
    void* ptr1 = _malloc(QUERY1);
    void* ptr2 = _malloc(QUERY2);

    struct block_header* header = block_get_header(ptr2);

    printf("State of heap before freeing:\n");
    debug_heap(stdout, heap);

    printf("Freeing 3rd block (capacity = 512)...\n");
    _free(ptr2);

    if (!header->is_free) {
        printf("The 3rd block failed to free!\n");
        return false;
    }

    printf("State of heap after freeing:\n");
    debug_heap(stdout, heap);

    _free(ptr0);
    _free(ptr1);

    printf("==> Test 2 passed.\n\n");
    return true;
}

 bool test3 (void* heap) {
    /**
     * In this test, we will test our own malloc by allocating 3 block of heap memory. Then we will free the 2nd and the 1st block.
     * After freeing, the 1st and 2nd block should be merged together into a bigger block.
     */
    printf("Test 3: Free 2 blocks\n");

    void* ptr0 = _malloc(QUERY0);
    void* ptr1 = _malloc(QUERY1);
    void* ptr2 = _malloc(QUERY2);

    struct block_header* header0 = block_get_header(ptr0);
    struct block_header* header1 = block_get_header(ptr1);

    printf("State of heap before freeing:\n");
    debug_heap(stdout, heap);

    printf("Freeing the 2nd block...\n");
    _free(ptr1);
    printf("Freeing the 1st block...\n");
    _free(ptr0);

    if (!(header0->is_free && header1->is_free)) {
        printf("The first and second block failed to free.\n");
        return false;
    }

    printf("State of heap after freeing:\n");
    debug_heap(stdout, heap);

    _free(ptr2);

    printf("==> Test 3 passed.\n\n");
    return true;
}

 bool test4(void* heap) {
    /**
     * In this test, we will test our own allocator by requesting a big amount of memory to see if the allocator
     * can expand the heap region by itself.
     * The expanded region may sit next to the old region. So the allocator should handle the transparent merge of them too.
     * The result we expected is the address of blocks sit next to each other, without the gap between them.
     */
    printf("Test 4: Expand the heap (continuously situation)\n");

    void* ptr0 = _malloc(5000);
    printf("State of the heap before expand:\n");
    debug_heap(stdout, heap);

    void* prt1 = _malloc(10000);
    printf("State of the heap after expand:\n");
    debug_heap(stdout, heap);

    struct block_header* block1 = block_get_header(ptr0);
    struct block_header* block2 = block_get_header(prt1);

    // Check if the heap expand success or failed
    if(block1->next != block2) {
        printf("Test failed: Block 1 and block 2 are not connected.\n");
        return false;
    }

    // Free them.
    _free(ptr0);
    _free(prt1);

    printf("==> Test 4 passed.\n\n");
    return true;
}

 bool test5(void* heap) {
    /**
     * In this test, we will check if our allocator can handle the discontinuous memory regions situation.
     * After allocating a block of memory, we use mmap directly to request for 30000 bytes, but we don't use it.
     * The purpose of this action is to create a gap between our memory regions, so as to make it discontinuous.
     * The result we expect is the address of the 3rd block is far away from the 1st block and 2nd block (because there a gap between them)
     */
    printf("Test 5: Memory regions are not continuous\n");

    void* ptr1 = _malloc(QUERY1);
    void* ptr2 = _malloc(QUERY2);

    printf("State of the heap before allocating 3rd block:\n");
    debug_heap(stdout, heap);

    // We prepare for making a mmap call
    struct block_header* last_block = block_get_header(ptr2);
    while (last_block->next != NULL)
        last_block = last_block->next;
    // We request 30000 bytes (maybe more due to round to size of a page (4kb)) from mmap directly. So our allocator won't know about this.
    void* new_region_addr = (void *) (last_block->contents + last_block->capacity.bytes);
    void *new_region = mmap(new_region_addr, 30000,
                     PROT_READ | PROT_WRITE,
                     MAP_PRIVATE | MAP_ANONYMOUS,
                           0, 0);


    if (new_region == MAP_FAILED) {
        printf("Test failed: Request memory directly from mmap failed.\n");
        return false;
    }

    // Request 30000 bytes from our own malloc.
    void* ptr3 = _malloc(30000);
    struct block_header* new_block = block_get_header(ptr3);

    if (new_block->is_free == true) {
        printf("Test failed: New block must be busy.\n");
        return false;
    }

    printf("State of the heap after expand the heap to a separated region:\n");
    debug_heap(stdout, heap);

    _free(ptr1);
    _free(ptr2);
    _free(ptr3);

    printf("==> Test 5 passed.\n\n");
    return true;
}

bool test6(void* heap) {
    /**
     * In this test, we will test our own realloc function in case the following blocks is mergeable.
     */
    printf("Test 6: Simple reallocation\n");

    // Let's allocate some memory
    void* ptr0 = _malloc(QUERY1);
    void* ptr1 = _malloc(QUERY1);
    void *ptr2 = _malloc(QUERY1);

    if (ptr0 == NULL || ptr1 == NULL || ptr2 == NULL) err("Allocation failed. Received NULL pointer .\n");

    // Print the map of the heap before reallocating
    printf("State of heap before reallocating:\n");
    debug_heap(stdout, heap);

    // Free the ptr1
    _free(ptr1);

    // Reallocate ptr0 with twice size
    ptr0 = _realloc(ptr0, QUERY1*2);

    // Check if reallocation success
    if (ptr0 > ptr2) {
        printf("Reallocation failed. Ptr0 must not point to a new address");    
        return false;
    }

    // Print the map of the heap
    printf("State of heap after reallocating:\n");
    debug_heap(stdout, heap);

    // Free the allocated memory
    _free(ptr0);
    _free(ptr2);

    printf("==> Test 6 passed.\n\n");
    return true;
}

bool test7(void* heap) {
    /**
     * In this test, we will test our own realloc function in case the following blocks is NOT mergeable.
     */
    printf("Test 7: Simple reallocation 2\n");

    // Let's allocate some memory
    void* ptr0 = _malloc(QUERY1);
    void* ptr1 = _malloc(QUERY1);
    void *ptr2 = _malloc(QUERY1);

    if (ptr0 == NULL || ptr1 == NULL || ptr2 == NULL) err("Allocation failed. Received NULL pointer .\n");

    // Print the map of the heap before reallocating
    printf("State of heap before reallocating:\n");
    debug_heap(stdout, heap);

    // Free the ptr1
    _free(ptr1);

    // Reallocate ptr0 with twice size
    ptr0 = _realloc(ptr0, QUERY1*3);

    // Check if reallocation success
    if (ptr0 < ptr2) {
        printf("Reallocation failed. Ptr0 must not point to the old address");    
        return false;
    }

    // Print the map of the heap
    printf("State of heap after reallocating:\n");
    debug_heap(stdout, heap);

    // Free the allocated memory
    _free(ptr0);
    _free(ptr2);

    printf("==> Test 7 passed.\n\n");
    return true;
}
