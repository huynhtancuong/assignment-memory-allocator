#include <stdarg.h>
#define _DEFAULT_SOURCE
#include <unistd.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <assert.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );



static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , 0, 0 );
  // flag MAP_ANONYMOUS: 
  // - This flag is used to create an anonymous mapping. 
  //   Anonymous mapping means the mapping is not connected to any files. 
  //   This mapping is used as the basic primitive to extend the heap
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region  ( void const * addr, size_t query ) {
  // Check the valid of pointer addr
  if (!addr) return REGION_INVALID;
  // Initialize some variables
  struct region alloc_region; 
  void* region_addr = MAP_FAILED;
  // First, we need to take the actual size of the query (it will be rounded to the size of a page, which is 4Kb)
  const size_t actual_size =  region_actual_size(query);
  // Next, we take the actual_size as block size for this big block.
  const block_size block_sz = {.bytes = actual_size};
  // Try to mmap at the specific address by using the flag MAP_FIXED_NOREPLACE. Don't use flag MAP_FIXED! It's will force mmap to map to this addr even there was another one there.
  region_addr = map_pages(addr, actual_size, MAP_FIXED_NOREPLACE);
  // If failed, then mmap at whatever address (we don't care about the address where it is mapped)
  if (region_addr == MAP_FAILED) {
    region_addr = map_pages(addr, actual_size, 0); // 'addr' here is just a hint to the mmap. It is not forced to allocate the this addr anymore.
    // If still failed, then return the REGION_INVALID, the reason is maybe we ran out of physical memory? I guessed.
    if (region_addr == MAP_FAILED) return REGION_INVALID;
  }
  // After these lines above, the region_addr should be != MAP_FAILED here.
  alloc_region = (struct region) { .addr = region_addr, .size = actual_size, .extends = false };
  block_init(region_addr, block_sz, NULL);
  return alloc_region;
}

static void* block_after( struct block_header const* block );

void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;

  return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big( struct block_header* block, size_t query ) {
  // First, we check if the block is big enough to split.
  // Then we split the big block into 2 smaller blocks.
  // The first block:   capacity = query
  // The second block:  capacity = capacity_of_big_block - query
  if (block_splittable(block, query)) {
    // Calculate the address of the second block. Address of second block = address_of_first_block +  a gap.
    // We use uint8_t here because it is the size of a pointer in 64-bit system.
    void *second_block = (void*)((uint8_t*) block + offsetof(struct block_header, contents) + query);
    // Then we calculate the block_size of the second block = capacity of big block - query
    const block_size second_block_sz = (block_size) { .bytes = block->capacity.bytes - query };
    // Next, we initialize the second block
    block_init(second_block, second_block_sz, block->next);
    // Finally, we recalculate the parameters of the first block.
    block->next = second_block;
    block->capacity.bytes = query;
    
    return true;
  }
  return false;
}


/*  --- Слияние соседних свободных блоков --- */

static void* block_after( struct block_header const* block )              {
  return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

static bool try_merge_with_next( struct block_header* block ) {
  // Check if the block and the block after it are mergable.
  if ( !block || !block->next ) return false;
  if (mergeable(block, block->next)) {
    // If they are mergeable, then we expand the size of the first block overlap the place of the second block.
    block->capacity.bytes += size_from_capacity(block->next->capacity).bytes;
    block->next = block->next->next;
    return true;
  }
  return false;
}


/*  --- ... ecли размера кучи хватает --- */

enum search_result {
  BSR_FOUND_GOOD_BLOCK, 
  BSR_REACHED_END_NOT_FOUND, 
  BSR_CORRUPTED
};

struct block_search_result {
  enum search_result type;
  struct block_header* block;
};

// struct block_search_result creator
static struct block_search_result block_search_result_create(enum search_result type, struct block_header* block) {
  if (type == BSR_CORRUPTED)
    return (struct block_search_result) { .type = type, .block = NULL };
  return (struct block_search_result) { .type = type, .block = block };
}


static struct block_search_result find_good_or_last  ( struct block_header* restrict block, size_t sz )    {
  // First, we check if the block is valid or not.
  if (!block) return block_search_result_create(BSR_CORRUPTED, NULL);
  // To find the good block, we need to loop through all the free block. At the end of the heap, block->next = NULL
  while (block) {
    // If we meet a busy block, then skip it. We don't want to mess with it.
    if ((block->is_free == false) && (block->next)) {
      block = block->next;
      continue;
    }
    // After this, the block should be free here.

    // We merge the block while loop through it. 
    // Because maybe there are some freed block next to each other. 
    // So we have to use a while loop here. The try will end when we had merged all of them into one.
    while(try_merge_with_next(block)); 
    // Then we split it so as to fit with our request
    split_if_too_big(block, sz);
    // Then we check if the block if big enough. If yes, then return.
    if (block_is_big_enough(sz, block)) {
      return block_search_result_create(BSR_FOUND_GOOD_BLOCK, block);
    }
    if (block->next) block = block->next;
    else break;
  }
  // Return the last block if the good block not found.
  return block_search_result_create(BSR_REACHED_END_NOT_FOUND, block);
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
  const struct block_search_result result = find_good_or_last(block, query);
  return result;
}



static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
  // Check if the last pointer is valid
  if (!last) return NULL;
  // First, we try to get the new region next to the last one. So we transparently merged 2 region.
  struct region new_region = alloc_region(block_after(last), query);
  // Check the valid of new region
  if (region_is_invalid(&new_region)) return NULL;
  // Then we link the last block of the old region to the first block of the new region
  last->next = new_region.addr;
  // In case 2 regions are next to each other, we to merge the last block of the first region to the new region.
  if (try_merge_with_next(last)) return last;
  // If we couldn't, then return the link to the new region.
  return last->next;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc( size_t query, struct block_header* heap_start) {
  const size_t actual_query = size_max(query, BLOCK_MIN_CAPACITY);
  // Find a good block from the HEAP_START
  struct block_search_result result = try_memalloc_existing(actual_query, heap_start);
  struct block_header *block = result.block;

  if (result.type == BSR_REACHED_END_NOT_FOUND) {
    block = grow_heap(block, actual_query);
    // In case grow heap failed
    if (!block) block = heap_init(actual_query); 
  }
  else if (result.type == BSR_CORRUPTED) {
    block = heap_init(actual_query);
  }

  // If result.type == BSR_FOUND_GOOD_BLOCK
  split_if_too_big(block, actual_query); // In case after grow heap.
  block->is_free = false;
  return block;

}

void* _malloc( size_t query ) {
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  else return NULL;
}

struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
  if (!mem) return ;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;
  // Try to merge it with next free blocks
  while (try_merge_with_next(header));
}

/**
 * Additional implementation of realloc
*/

/// @brief attempts to resize the memory block pointed to by mem that 
/// was previously allocated with a call to malloc or calloc.
/// @param mem This is the pointer to a memory block previously allocated 
/// with malloc, calloc or realloc to be reallocated. If this is NULL, 
/// a new block is allocated and a pointer to it is returned by the function.
/// @param size This is the new size for the memory block, in bytes. 
/// If it is 0 and ptr points to an existing block of memory, 
/// the memory block pointed by ptr is deallocated and a NULL pointer is returned.
/// @return This function returns a pointer to the newly allocated memory, or NULL if the request fails.
void* _realloc(void* mem, size_t size) {
  if (!mem) {
    if (!size) return NULL;
    return _malloc(size);
  }
  if ((mem) && (!size)) {
    _free(mem);
    return NULL;
  }
  struct block_header* header = block_get_header(mem);
  // We try to merge the current block with its following blocks
  while(try_merge_with_next(header));
  // Then try to split it if it too big for the requested size
  split_if_too_big(header, size);
  // if expand successfully then return the contents of the header 
  if (header->capacity.bytes >= size) { 
    return header->contents;
  }
  else { 
  // if expand failed, we free the current
    _free(mem);
    return _malloc(size);
  }
}
