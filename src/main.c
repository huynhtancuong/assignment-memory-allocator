#include "test.h"
#define HEAP_INIT 10000

int main() {
    // First, let's get a big chunk of memory from mmap. Then see how our program perform.
    printf("Initialize Heap:\n");
    void * heap = heap_init(HEAP_INIT);
    if (!heap) err("Heap initialization failed.\n");
    debug_heap(stdout, heap);
    // Perform some tests on our own malloc
    if (test1(heap) && test2(heap) && test3(heap) && test4(heap) && test5(heap) && test6(heap) && test7(heap)) {
        printf("------RESULT------\n");
        printf("=> CONGRAGULATION! ALL TESTS PASSED\n");
        return 0;
    }
    // test5(heap);
    return 1;
}